package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.domain.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController //自动返回json数据
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping()
    public List<Employee> getAllEmployee() {
        return employeeRepository.getAllEmployee();
    }

    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable int id) {
        return employeeRepository.getEmployeeById(id);
    }

    @GetMapping(value = "", params = "gender") //表示请求要带一个param
    public List<Employee> getEmployeeByGender(@RequestParam String gender) {
        return employeeRepository.getEmployeeByGender(gender);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public int addEmployee(@RequestBody Employee employee) { //将json映射成类
        return employeeRepository.addEmployee(employee);
    }


    @PutMapping("/{id}")
    public Employee updateEmployees(@PathVariable Integer id,@RequestBody Employee newEmployee) {
        return employeeRepository.updateEmployees(id, newEmployee);
    }

    @DeleteMapping("/{id}")
    public Integer deleteEmployee(@PathVariable int id) {
        return employeeRepository.deleteEmployee(id);
    }

    @GetMapping("/page")
    public List<Employee> getPageEmployee(@RequestParam int page, @RequestParam int size) {
        return employeeRepository.getPageEmployee(page, size);
    }
}
