package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.domain.Company;
import com.thoughtworks.springbootemployee.domain.Employee;
import com.thoughtworks.springbootemployee.dto.CompanyWithEmployeesDto;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping()
    public List<Company> getCompanies() {
        return companyRepository.getAllCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable int id) {
        return companyRepository.getCompanyById(id);
    }


    @GetMapping(value = "/{id}/employee")
    public CompanyWithEmployeesDto getEmployeeByCompanyId(@PathVariable int id) {
        Company company = companyRepository.getCompanyById(id);
        List<Employee> employees = employeeRepository.getEmployeeByCompanyId(id);
        CompanyWithEmployeesDto dto = new CompanyWithEmployeesDto();
        dto.setCompany(company);
        dto.setEmployees(employees);
        return dto;
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public int addCompany(@RequestBody Company Company) {
        return companyRepository.addCompany(Company);
    }


    @PutMapping("/{id}")
    public Company updateCompany(@PathVariable int id, @RequestBody Company newCompany) {
        return companyRepository.updateCompany(id, newCompany);
    }

    @DeleteMapping("/{id}")
    public Integer deleteCompany(@PathVariable int id) {
        return companyRepository.deleteCompany(id);
    }

    @GetMapping("/page")
    public List<Company> getPageCompany(@RequestParam int page, @RequestParam int size) {
        return companyRepository.getPageCompany(page, size);
    }
}
