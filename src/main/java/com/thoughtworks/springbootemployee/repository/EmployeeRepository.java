package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.domain.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    List<Employee> employees = new ArrayList<>();

    public EmployeeRepository() {
        employees.add(new Employee(1, "Lily", 20, "Female", 8000,1));
    }

    public List<Employee> getAllEmployee() {
        return employees;
    }


    public Employee getEmployeeById(int id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getEmployeeByCompanyId(int companyId) {

        return employees.stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public int addEmployee(Employee employee) { //将json映射成类
        employee.setId(nextId());
        employees.add(employee);
        return employee.getId();
    }

    public Employee updateEmployees(Integer id, Employee newEmployee) {
        Employee updatedEmployee = employees.stream()
                .filter(emp -> emp.getId().equals(id))
                .findFirst().orElse(null);
        if (updatedEmployee != null) {
//            BeanUtils.copyProperties(newEmployee,updatedEmployee);
            updatedEmployee.setAge(newEmployee.getAge());
            updatedEmployee.setSalary(newEmployee.getSalary());
        }
        return updatedEmployee;
    }

    public Integer deleteEmployee(int id) {
        employees.stream()
                .filter(employee1 -> employee1.getId().equals(id))
                .findFirst()
                .ifPresent(employee -> employees.remove(employee));
        return id;
    }

    public List<Employee> getPageEmployee(int page, int size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    private int nextId() {
        int currentMaxId = employees.stream()
                .mapToInt(Employee::getId)
                .max()
                .orElse(0);
        return currentMaxId + 1;
    }
}
