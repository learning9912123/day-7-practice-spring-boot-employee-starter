package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.domain.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {

    List<Company> companies = new ArrayList<>();

    public CompanyRepository() {
        companies.add(new Company(1, "cargo smart"));
    }

    public List<Company> getAllCompanies() {
        return companies;
    }


    public Company getCompanyById(int id) {
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);

    }


    public int addCompany(Company company) { //将json映射成类
        company.setId(nextId());
        companies.add(company);
        return company.getId();
    }

    public Company updateCompany(int id, Company newcompany) {
        Company updatedcompany = companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst().orElse(null);
        if (updatedcompany != null) {
            updatedcompany.setName(newcompany.getName());
        }
        return updatedcompany;
    }

    public Integer deleteCompany(int id) {
        companies.stream()
                .filter(company1 -> company1.getId().equals(id))
                .findFirst()
                .ifPresent(company -> companies.remove(company));
        return id;
    }

    public List<Company> getPageCompany(int page, int size) {
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    private int nextId() {
        int currentMaxId = companies.stream()
                .mapToInt(Company::getId)
                .max()
                .orElse(0);
        return currentMaxId + 1;
    }


}
