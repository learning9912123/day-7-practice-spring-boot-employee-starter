package com.thoughtworks.springbootemployee.dto;

import com.thoughtworks.springbootemployee.domain.Company;
import com.thoughtworks.springbootemployee.domain.Employee;

import java.util.List;

public class CompanyWithEmployeesDto {
    private Company company;
    private List<Employee> employees;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
